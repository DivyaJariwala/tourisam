import {
  useFocusEffect,
  useIsFocused,
  useNavigation,
} from "@react-navigation/native";
import React, { useCallback, useContext, useEffect } from "react";
import {
  Dimensions,
  FlatList,
  LogBox,
  ScrollView,
  Text,
  View,
} from "react-native";
import FastImage from "react-native-fast-image";
import { useDispatch, useSelector } from "react-redux";
import { StatusbarContext } from "../../StatusbarContext";
import UserIcon from "../../assets/image/profile.png";
import AppButtonComponent from "../../components/AppButtonComponent";
import GradientTextComponent from "../../components/GradientTextComponent";
import NoDataComponent from "../../components/NoDataComponent";
import TitleComponent from "../../components/TitleComponent";
import TopNavBar from "../../components/TopNavBarComponent";
import TopSpotComponent from "../../components/TopSpotComponent";
import TravelGuideComponent from "../../components/TravelGuideComponent";
import {
  fetchActvitiesFailure,
  fetchActvitiesSuccess,
} from "../../redux/activities/actions";
import { callApi } from "../../redux/apiService";
import { ActivitiesItem, ActivityItem } from "../../redux/interfaces";
import { RootState } from "../../redux/rootReducer";
import { styles } from "./styles";

export default function SurfingScreen({ route }: { route: any }) {
  const activity_type = route.params?.screen;
  const isFocused = useIsFocused();
  const navigation = useNavigation();
  const { color, setColor } = useContext(StatusbarContext);
  const dispatch = useDispatch();

  const activityData = useSelector<RootState, ActivityItem>(
    (state) => state.activitiesData
  );

  useFocusEffect(
    useCallback(() => {
      setColor("#008080");
    }, [])
  );

  useEffect(() => {
    LogBox.ignoreLogs(["VirtualizedLists should never be nested"]);
    const unsubscribe = navigation.addListener("focus", () => {
      if (isFocused) {
        // updateReceivedData("red");
        callApi<ActivityItem>(
          `activities/${activity_type}`,
          fetchActvitiesSuccess,
          fetchActvitiesFailure,
          dispatch
        );
      }
    });
    return unsubscribe;
  }, [isFocused, dispatch]);

  const handleTopSpotPress = (item: string) => {
    console.log(item, "top spot pressed!");
  };

  const renderTopSpotItem = ({
    item,
    index,
  }: {
    item: ActivitiesItem;
    index: number;
  }) => (
    <TopSpotComponent
      index={`${index + 1}.`}
      title={item.name}
      onPress={() => handleTopSpotPress(item.name)}
    />
  );

  return (
    <View style={styles.mainContainerStyle}>
      {/* TopNavBar */}
      <TopNavBar
        title="Aloha"
        // onBackPress={handleButtonPress}
      />

      {/* ScrollView */}
      <ScrollView
        contentContainerStyle={styles.scrollviewStyle}
        showsVerticalScrollIndicator={false}
      >
        {/* Image with text */}
        <View>
          <FastImage
            style={styles.imageStyle}
            source={{ uri: activityData.image }}
          >
            <GradientTextComponent title={activityData.name} />
          </FastImage>
        </View>

        {/* Description view */}
        <View style={{ backgroundColor: "white" }}>
          <View style={styles.textContainerStyle}>
            <Text style={styles.descStyle}>{activityData.description}</Text>
          </View>

          {/* Header: Top spot view */}
          <View style={styles.wrapperViewStyle}>
            {/* Header: Top spot view */}
            <TitleComponent title="Top spots" />
            {/* Header: Top spot list */}
            <FlatList
              contentContainerStyle={{ paddingHorizontal: 16 }}
              data={activityData.activities}
              renderItem={renderTopSpotItem}
              ListEmptyComponent={NoDataComponent}
            />
          </View>
        </View>

        {/* Header: Travel Guide view */}
        <View style={[styles.wrapperViewStyle, { backgroundColor: "#E2F0F0" }]}>
          {/* Header: Travel Guide */}
          <TitleComponent title="Travel Guide" />
          {/* Header: Travel contact view */}
          <TravelGuideComponent
            title="Hadwin Malone"
            desc="Guide since 2012"
            image={UserIcon}
            onPress={() => console.log("Contact press")}
          />
        </View>
      </ScrollView>
      <View
        style={{
          marginHorizontal: 16,
          position: "absolute",
          bottom: 16,
          width: Dimensions.get("window").width - 32,
        }}
      >
        <AppButtonComponent
          title="Book a trip"
          onPress={() => console.log("Book a trip clicked")}
        />
      </View>
    </View>
  );
}
