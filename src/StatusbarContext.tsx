import { createContext } from "react";

interface StatusbarContextProps {
  color: string;
  setColor: (data: string) => void;
}

export const StatusbarContext = createContext<StatusbarContextProps>({
  color: "blue",
  setColor: () => {},
});
