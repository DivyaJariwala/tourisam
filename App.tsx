import { NavigationContainer } from "@react-navigation/native";
import React, { useState } from "react";
import { SafeAreaView, StatusBar, StyleSheet } from "react-native";
import { Provider } from "react-redux";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { StatusbarContext } from "./src/StatusbarContext";
import rootReducer from "./src/redux/rootReducer";
import BottomTab from "./src/router/BottomTabNavigator";

const store = createStore(rootReducer, applyMiddleware(thunk));

function App(): JSX.Element {
  const [color, setColor] = useState("");

  return (
    <Provider store={store}>
      <StatusbarContext.Provider value={{ color, setColor }}>
        <SafeAreaView style={{ flex: 0, backgroundColor: color }} />
        <SafeAreaView style={{ flex: 1 }}>
          <StatusBar
            backgroundColor={color}
            barStyle={color === "#008080" ? "light-content" : "light-content"}
          />
          <NavigationContainer>
            <BottomTab />
          </NavigationContainer>
        </SafeAreaView>
      </StatusbarContext.Provider>
    </Provider>
  );
}

const styles = StyleSheet.create({});

export default App;
